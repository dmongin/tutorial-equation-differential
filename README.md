# tutorial equation differential

This is the R code used to generate the material used for the article "A tutorial on ordinary differential equations in behavioral science: what do physics teach us?" 


- [generate_data.R ](./generate_data.R) is the file containing the functions generating simple solutions of first and second order differential equation for a given set of parameters (using numerical integration)
- [figure.R](./figure.R) contains the R code to generate all the figure of the article.

